# README #

This Project extracts noun phrases from a paragraph. It uses [OpenNlp](https://opennlp.apache.org/).

### It executes the following things chronologically. ###

* Remove the emoticons.
* Extract the parts of speech. 
* Extract the noun phrases.
* Remove stop words.