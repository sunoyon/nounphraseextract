package nlp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import nlp.data.MappingTag;
import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

import org.apache.commons.io.IOUtils;

public class NounPhraseProcess {

	private Set<String> stopWords = null;
	private ChunkerModel myChunkerModel = null;
	private POSModel myPosModel = null;
	private TokenizerModel myTokenModel = null;

	private static final NounPhraseProcess myInstance = new NounPhraseProcess();

	private static final String SPACE_EXCEPTIONS = "\\n\\r";
	private static final String SPACE_CHAR_CLASS = "\\p{C}\\p{Z}&&[^"
			+ SPACE_EXCEPTIONS + "\\p{Cs}]";
	private static final String SPACE_REGEX = "[" + SPACE_CHAR_CLASS + "]";
	private static final String PUNCTUATION_CHAR_CLASS = "\\p{P}\\p{M}\\p{S}"
			+ SPACE_EXCEPTIONS;
	private static final String PUNCTUATION_REGEX = "["
			+ PUNCTUATION_CHAR_CLASS + "]";
	private static final String SMILEY_REGEX_PATTERN = ":[)DdpP]|:[ -]\\)|<3";
	private static final String EMOTICON_DELIMITER = SPACE_REGEX + "|"
			+ PUNCTUATION_REGEX;
	private static final String FROWNY_REGEX_PATTERN = ":[(<]|:[ -]\\(";

	private static final String EMOC_REGEX = "(?<=^|" + EMOTICON_DELIMITER
			+ ")(" + SMILEY_REGEX_PATTERN + "|" + FROWNY_REGEX_PATTERN
			+ ")+(?=$|" + EMOTICON_DELIMITER + ")";

	private static final String HTML_TAG_REGEX = "<[^>]+>" + "|"
			+ "&lt;/?[a-z0-9-_]+&gt;";

	private NounPhraseProcess() {
		InputStream modelIn = null;
		InputStream posModelIn = null;
		InputStream chunkerModelIn = null;
		try {
			stopWords = new HashSet<String>();

			modelIn = this.getClass().getClassLoader()
					.getResourceAsStream("en-token.bin");
			myTokenModel = new TokenizerModel(modelIn);
			posModelIn = this.getClass().getClassLoader()
					.getResourceAsStream("en-pos-maxent.bin");
			myPosModel = new POSModel(posModelIn);
			chunkerModelIn = this.getClass().getClassLoader()
					.getResourceAsStream("en-chunker.bin");
			myChunkerModel = new ChunkerModel(chunkerModelIn);
			createStopWords();
		} catch (Exception e) {
			throw new RuntimeException("Error in NounPhraseProcess.", e);
		} finally {
			close(modelIn);
			close(posModelIn);
			close(chunkerModelIn);
		}
	}

	private void close(InputStream in) {
		IOUtils.closeQuietly(in);
	}

	public static NounPhraseProcess getInstance() throws IOException,
			URISyntaxException {
		return myInstance;
	}

	public List<String> getNounPhrase(String aMessage) {
		String aRemovedEmoticonMsg = removeEmoticons(aMessage.toLowerCase());
		return removeStopWords(generateNounPhrasesFrom(chunker(posTagger(getTokens(aRemovedEmoticonMsg
				.toLowerCase())))));
	}

	private String removeEmoticons(String aMessage) {
		String emotion = ":[\\w_]+:|" + EMOC_REGEX;
		aMessage = aMessage.replaceAll(emotion, ". ");
		String regx = HTML_TAG_REGEX;
		return aMessage.replaceAll(regx, ". ");
	}

	private void createStopWords() throws IOException, URISyntaxException {

		String[] fileNames = { "stop-words_english_1_en.txt",
				"stop-words_english_2_en.txt", "stop-words_english_3_en.txt",
				"stop-words_english_4_google_en.txt",
				"stop-words_english_5_en.txt", "stop-words_english_6_en.txt" };
		for (String aFileName : fileNames) {
			InputStream in = null;
			InputStreamReader inputStreamReader = null;
			BufferedReader bufferedReader = null;
			try {
				in = NounPhraseProcess.class.getResourceAsStream("/stop-words/"
						+ aFileName);
				inputStreamReader = new InputStreamReader(in, "UTF-8");
				bufferedReader = new BufferedReader(inputStreamReader);
				while (bufferedReader.ready()) {
					String aLine = bufferedReader.readLine().trim();
					if (aLine != "") {
						stopWords.add(aLine);
					}
				}

			} finally {
				bufferedReader.close();
				inputStreamReader.close();

			}
		}
	}

	private List<String> removeStopWords(List<List<String>> someNps) {
		List<List<String>> someSubtractNps = new ArrayList<List<String>>();
		for (List<String> aNp : someNps) {
			Set<String> aNpSet = new LinkedHashSet<String>(aNp);
			aNpSet.removeAll(stopWords);
			if (aNpSet.size() > 0) {

				Set<String> aNewNpSet = new LinkedHashSet<String>();
				int aNumCount = 0;
				for (String aText : aNpSet) {

					// Replace everything other than a-z and 0-9
					// with space.
					aText = aText.replaceAll("[^a-z0-9]", " ")
							.replaceAll("\\s+", " ").trim();
					if (!stopWords.contains(aText)) {

						// Mark how many are numbers.
						// We do not want to take only the numbers.
						if (aText.replaceAll("[0-9\\s]+", "").trim() == "") {
							aNumCount++;
						}

						aNewNpSet.add(aText);
					}
				}
				if (aNewNpSet.size() > aNumCount) {
					List<String> aLine = new ArrayList<String>(aNewNpSet);
					someSubtractNps.add(aLine);
				}
			}
		}
		List<String> someOfNounPhrases = new ArrayList<String>();
		for (List<String> listOfNounPhrases : someSubtractNps) {
			someOfNounPhrases.add(toString(listOfNounPhrases, " "));
		}
		return someOfNounPhrases;
	}

	private List<List<String>> generateNounPhrasesFrom(MappingTag theMappingTag) {
		String[] someTokens = theMappingTag.getMyTokens();
		String[] someTags = theMappingTag.getMyTokenMappedTags();
		List<List<String>> someListOfNounPhrases = new LinkedList<List<String>>();
		List<String> someNounPhrases = null;
		for (int i = 0; i < someTokens.length; i++) {
			String aTopic = someTokens[i];
			String aTag = someTags[i];
			if (aTag.equals("B-NP")) {
				someNounPhrases = new LinkedList<String>();
				someNounPhrases.add(aTopic);
				someListOfNounPhrases.add(someNounPhrases);
				continue;
			}
			if (aTag.equals("I-NP")) {
				someNounPhrases.add(aTopic);
			} else {
				if (someNounPhrases != null && someNounPhrases.size() == 1) {
					someListOfNounPhrases.remove(0);
					someNounPhrases = null;
				}
			}
		}
		return someListOfNounPhrases;
	}

	private String toString(Collection<String> collection, String seperator) {
		if (collection == null) {
			return "";
		}

		String result = "";
		for (Iterator<String> iterator = collection.iterator(); iterator
				.hasNext();) {
			String object = iterator.next();
			result += object;
			if (iterator.hasNext()) {
				result += seperator;
			}
		}
		return result.replaceAll("_+", "_");
	}

	private MappingTag chunker(MappingTag thePostagged) {
		ChunkerME chunker = new ChunkerME(myChunkerModel);
		return new MappingTag(thePostagged.getMyTokens(),
				chunker.chunk(thePostagged.getMyTokens(),
						thePostagged.getMyTokenMappedTags()));
	}

	private MappingTag posTagger(String[] theTokens) {
		POSTaggerME tagger = new POSTaggerME(myPosModel);
		String tags[] = tagger.tag(theTokens);
		return new MappingTag(theTokens, tags);
	}

	private String[] getTokens(String theMessage) {
		Tokenizer tokenizer = new TokenizerME(myTokenModel);
		return tokenizer.tokenize(theMessage);
	}
}
