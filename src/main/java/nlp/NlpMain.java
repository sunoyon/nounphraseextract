package nlp;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class NlpMain {

	public static void main(String[] args) throws IOException,
			URISyntaxException {
		// String aMessage =
		// "Opera was always           known for making extremely effective and fast, through built in proxy compression of web-pages, mobile-browsers for the low end market during my Nokia years, so if they've managed to utilize that technology for mobile video then they will definitely be a strong player. As of the Nokia App store, not sure, but I think they had their own Browser plugin app store for a short while during that period too.";
		// String aMessage = "This <a id='test'>Test anchor</a>is test :) :(";
		String aMessage = "Sales team wants to send them realtime data.";
		List<String> nounPhrase = NounPhraseProcess.getInstance()
				.getNounPhrase(aMessage);
		for (String string : nounPhrase) {
			System.out.println(string);
		}
	}

}
