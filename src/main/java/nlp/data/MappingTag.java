package nlp.data;

public class MappingTag {
	String []myTokens;
	String []myTokenMappedTags;
	
	public MappingTag(String[] myTokens, String[] myTokenMappedTags) {
		this.myTokens = myTokens;
		this.myTokenMappedTags = myTokenMappedTags;
	}
	public String[] getMyTokens() {
		return myTokens;
	}
	public String[] getMyTokenMappedTags() {
		return myTokenMappedTags;
	}
	public void setMyTokens(String[] myTokens) {
		this.myTokens = myTokens;
	}
	public void setMyTokenMappedTags(String[] myTokenMappedTags) {
		this.myTokenMappedTags = myTokenMappedTags;
	}
	
	

}
